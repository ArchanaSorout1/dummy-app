import 'package:assignment/base/controller/age_controller.dart';
import 'package:assignment/common/common_widgets.dart';
import 'package:assignment/helper/helper_class.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController nameController = TextEditingController();
  AgeController ageController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Age"),
        backgroundColor: Colors.orange,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            editTextField(
              nameController,
              false,
              "Please enter your name",
              false,
              onChanged: (value){

              }
            ),
            const SizedBox(height: 20,),
            button(context),
            const SizedBox(height: 20,),
            Obx(()=>showNameAgeWidget("Name",
                ageController.ageModel.value.name!=null?
            ageController.ageModel.value.name!:"")),
            const SizedBox(height: 10,),
                Obx(()=> showNameAgeWidget("Age",ageController.ageModel.value.age!=null?
              ageController.ageModel.value.age.toString():""),
            ),
          ],
        ),
      ),
    );
  }

 Widget showNameAgeWidget(String key, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          key,
          style: const TextStyle(
              color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        const SizedBox(
          width: 20,
        ),
        Text(
          value,
          style: const TextStyle(color: Colors.grey, fontSize: 18),
        ),
      ],
    );
  }

  button(BuildContext context) {
    return customButton(
      text: "Save",
      onTap: () async {
        if(nameController.text.isNotEmpty)
          {
            launchProgress();
            await ageController.getAge(name: nameController.text);
          disposeProgress();
          }
        else
          {
            Get.snackbar("Warning", "Please enter name",backgroundColor: Colors.grey,colorText: Colors.white);
          }
      },
    );
  }
}
