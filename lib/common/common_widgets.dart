import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';




Widget customButton({
  String? text,
  dynamic onTap,
  Color backgroundColor = Colors.orange,
  Color borderColor = Colors.orange,
  Color textColor = Colors.white,
}) {
  // ignore: deprecated_member_use
  return RaisedButton(
    padding: const EdgeInsets.all(15),
    color: backgroundColor,
    elevation: 0,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          text.toString(),
          textAlign: TextAlign.center,
          style: TextStyle(
             color: textColor,
            fontSize: 15,
          ),
        ),
      ],
    ),
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
        side: BorderSide(color: borderColor)),
    splashColor: Colors.black12,
    onPressed: onTap,
  );
}

customDialog({
  bool barrierDismissible = true,
  var isLoader = false,
  Widget widget = const Text('Pass sub widgets'),
}) async {
  var result = await Get.dialog(
      isLoader
          ? widget
          : Align(
              alignment: Alignment.center,
              child: Card(
                child: widget,
                margin: const EdgeInsets.only(bottom: 16, left: 16, right: 16, top: 25),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
              ),
            ),
      barrierDismissible: barrierDismissible);
  if (result != null) return result;
}


Widget editTextField(TextEditingController controller, bool isPasVisible,
    String hint, bool _passwordVisible,
    {Function? onPressed,
    int? length,
    int maxLines = 1,
    bool enableNumber = false,
    Widget? suffixWidget,
    ValueChanged<String>? onFieldSubmitted,
    ValueChanged<String>? onChanged,
    bool enabled = true}) {
  return Container(
    margin: const EdgeInsets.only(top: 10),
    decoration: BoxDecoration(
      color: Colors.white,
      border: Border.all(
        color: const Color(0xffDED6D2),
        width: 0.5,
      ),
      borderRadius: const BorderRadius.all(Radius.circular(8)),
    ),
    child: TextFormField(
        enabled: enabled,
        controller: controller,
        inputFormatters: [LengthLimitingTextInputFormatter(length)],
        textInputAction: TextInputAction.next,
        textAlignVertical: TextAlignVertical.center,
        obscureText: isPasVisible ? !_passwordVisible : false,
        keyboardType: enableNumber ? TextInputType.number : null,
        maxLines: maxLines,
        onChanged: (value) {
          if (onChanged != null) onChanged(value);
        },
        onFieldSubmitted: (value) {
          if (onFieldSubmitted != null) onFieldSubmitted(value);
        },
        decoration: InputDecoration(
          border: InputBorder.none,

          fillColor: Colors.transparent,
          filled: true,
          contentPadding:const EdgeInsets.only(left: 8,right: 8,top: 15,bottom: 15) ,
          hintText: hint,
          counterText: "",
          hintStyle: const TextStyle(
            fontSize: 15,
          ),
          //  border: InputBorder.none,
          suffixIcon: isPasVisible
              ? IconButton(
                  icon: Icon(
                    _passwordVisible ? Icons.visibility : Icons.visibility_off,
                    color: Colors.brown,
                  ),
                  onPressed: () {
                    onPressed!();
                  },
                )
              : null,
          suffix: suffixWidget,
        ),
        style:const TextStyle(
           fontSize: 15,
        )),
  );
}

