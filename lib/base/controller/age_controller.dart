import 'package:assignment/base/model/age_model.dart';
import 'package:assignment/base/services/repo/age_repo.dart';
import 'package:get/get.dart';

class AgeController{
  Rx<AgeModel> ageModel =AgeModel().obs ;
  AgeRepo ageRepo=AgeRepo();
  Future<AgeModel> getAge({required String name,}) async {
    final response = await ageRepo.getAgeApi(name: name);
    ageModel.value=response;
     return response;
  }

}