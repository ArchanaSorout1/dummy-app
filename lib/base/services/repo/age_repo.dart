
 import 'package:assignment/base/constant/api_endpoint.dart';
import 'package:assignment/base/model/age_model.dart';
import 'package:assignment/base/services/network/api_hitter.dart';

class AgeRepo {
   Future<AgeModel> getAgeApi({required String name,}) async {
    ApiResponse apiResponse = await ApiHitter().getApiResponse(
      ApiEndpoint.AGE_API+name,
     );
    if (apiResponse.status) {
      return AgeModel.fromJson(apiResponse.response!.data);
    } else {
      return AgeModel(name:"", age:0,count:0);
    }
  }

}

