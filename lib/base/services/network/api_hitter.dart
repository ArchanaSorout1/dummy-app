import 'package:assignment/base/constant/api_endpoint.dart';
import 'package:assignment/helper/helper_class.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ApiHitter {
  static Dio? _dio;

  Dio getDio(  {String baseurl = ''}) {
    if (_dio == null) {
      BaseOptions options = BaseOptions(
        baseUrl: baseurl.isEmpty ? ApiEndpoint.BASE_URL : baseurl,
        connectTimeout: 30000,
        receiveTimeout: 30000,
      );
      return   Dio(options)
        ..interceptors.add(InterceptorsWrapper(
            onRequest:(options, handler){
              return handler.next(options); //continue
            },
            onResponse:(response,handler) async {
              debugPrint("response::::" +response.toString());
              return handler.next(response); // continue
            },
            onError: (DioError e, handler) async {
               debugPrint("error response::::" + e.response.toString());
              debugPrint("error message::::" + e.message.toString());
              return handler.next(e);
            }
        ));

    } else {
      return _dio!;
    }
  }

  Future<ApiResponse> getPostApiResponse(
      String endPoint,
      {
        Map<String, dynamic>? headers,
        Map<String, dynamic>? data,
        String baseurl = '',
      }) async {
    bool value = await checkInternetConnection();
    if (value) {
      try {
        var response = await getDio(

          baseurl: baseurl,
        ).post(endPoint,
            options: Options(
              headers: headers,
              contentType: "application/json",
            ),
            data: data);
        return ApiResponse(response.statusCode == 200, response: response, msg: response.statusMessage!);
      } catch (error) {
        return exception(error,  );
      }
    } else {
      return ApiResponse(false, msg: "Check your internet connection and Please try again later.");
    }
  }

  Future<ApiResponse> getPutApiResponse(
      String endPoint,
      {
        Map<String, dynamic>? headers,
        Map<String, dynamic>? data,
        String baseurl = '',
      }) async {
    bool value = await checkInternetConnection();
    if (value) {
      try {
        var response = await getDio(

          baseurl: baseurl,
        ).put(endPoint,
            options: Options(
              headers: headers,
              contentType: "application/json",
            ),
            data: data);
        return ApiResponse(response.statusCode == 200,
            response: response, msg: response.statusMessage!);
      } catch (error) {
        return exception(error,  );
      }
    } else {
      return ApiResponse(
        false,
        msg: "Check your internet connection and Please try again later.",
      );
    }
  }

  Future<ApiResponse> getApiResponse(
      String endPoint,
      {
        Map<String, dynamic>? headers,
        Map<String, dynamic>? queryParameters,
        String baseurl = '',
      }) async {

    bool value = await checkInternetConnection();
    if (value) {
      try {
        var response = await getDio(
          baseurl: baseurl,
        ).get(
          endPoint,
          queryParameters: queryParameters,
          options: Options(
            headers: headers,
            contentType: "application/json",
          ),
        );
        return ApiResponse(response.statusCode == 200,
            response: response,msg: response.statusMessage!);
      } catch (error) {
        return exception(error,  );
      }
    } else {
      debugPrint("not connected ");

      return ApiResponse(false, msg: "Check your internet connection and Please try again later.",response: null);
    }
  }

  deleteApiResponse(
      String endPoint,
       {
        Map<String, dynamic>? headers,
        Map<String, dynamic>? data,
        String baseurl = '',
      }) async {
    bool value = await checkInternetConnection();
    if (value) {
      try {
        var response = await getDio(
          baseurl: baseurl,
        ).delete(
          endPoint,
          options: Options(
            headers: headers,
            contentType: "application/json",
          ),
        );
        return ApiResponse(response.statusCode == 200,
            response: response, msg: response.statusMessage!);
      } catch (error) {
        return exception(error,  );
      }
    } else {
      return ApiResponse(false,msg: "Check your internet connection and Please try again later.");
    }
  }


  Future<ApiResponse> getFormApiResponse(
      String endPoint,
      {FormData? data,
        Map<String, dynamic>? headers,
        Map<String, dynamic>? queryParameters}) async {
    try {
      var response = await getDio(
      ).post(endPoint,
          data: data,
          queryParameters: queryParameters,
          options: Options(
            headers: headers, contentType: 'multipart/form-data'
          ));
      return ApiResponse(response.statusCode == 200, response: response, msg: response.statusMessage!);
    } catch (error) {
      return exception(error,);
    }
  }

  exception(error,  ) {
    try {
      return  ApiResponse(error.response!.statusCode == 200, response: error.response, msg: error.response.data["message"]);
     } catch (e) {
      if (DioErrorType.other == error.type ||
          DioErrorType.receiveTimeout == error.type ||
          DioErrorType.connectTimeout == error.type) {
         if (error.message.contains('SocketException')) {
          return ApiResponse(false, msg: "Check your internet connection and Please try again later.");
        }
        else if(error.message.contains("Connecting timed out [30000ms]")){
          return ApiResponse(false, msg: "Please check your internet connection",);
        }
        else{
          return ApiResponse(false, msg: "Sorry Something went wrong.",);
        }
      } else {
        return ApiResponse(false, msg: "Sorry Something went wrong.");
      }
    }
  }
}

class ApiResponse {
  final bool status;
  final String msg;
  final Response ?response;

  ApiResponse(this.status, {this.msg = "Success",  this.response});
}
