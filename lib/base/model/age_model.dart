class AgeModel {
  String? name;
  int? age;
  int? count;

  AgeModel({this.name, this.age, this.count});

  AgeModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    age = json['age'];
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['name'] = name;
    data['age'] = age;
    data['count'] = count;
    return data;
  }
}
