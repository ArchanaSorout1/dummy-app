import 'dart:io';
import 'package:assignment/common/common_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Future<bool> checkInternetConnection() async {
  final List<InternetAddress> result;
  try {
    result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
       debugPrint('internet status connected');
      return true;
    }
  }
  on SocketException catch (_) {
    debugPrint('not connected');
    return false;
  }
  return result.isNotEmpty && result[0].rawAddress.isNotEmpty ? true : false;
}

bool checkValidEmail(String email) {
  bool emailValid = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email);
  return emailValid;
}

double screenWidth(BuildContext context) {
  return MediaQuery
      .of(context)
      .size
      .width;
}

double screenHeight(BuildContext context) {
  return MediaQuery
      .of(context)
      .size
      .height;
}


bool validateMobile(String value) {
  String pattern = r'(^(?:[+0]9)?[0-9]{10,15}$)';
  RegExp regExp =   RegExp(pattern);
  return regExp.hasMatch(value);
}

double getStatusBarHeight(BuildContext context) {
  return MediaQuery.of(context).padding.top;
}

bool isEmail(String em) {
  String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = RegExp(p);
  return regExp.hasMatch(em);
}


launchProgress({String message = 'Processing..',}) {
  customDialog(
      isLoader: true,
      barrierDismissible: false,
      widget: const Center(
          child: CircularProgressIndicator(
            valueColor:   AlwaysStoppedAnimation<Color>(Color(0xffFF6D41)),
            //   backgroundColor: Color(0xffDBB77C),
          )));
}

disposeProgress() {
  Get.back();
}

bool validateStructure(String value) {
  String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  RegExp regExp = RegExp(pattern);
  return regExp.hasMatch(value);
}




